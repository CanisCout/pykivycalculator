from gui import MainWindow as mw


def main(*args, **kwargs):
    mw.Calculator().run()
    return 1


if __name__ == "__main__":
    main()
