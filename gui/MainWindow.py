from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.config import Config

import calculator as calc

Config.set("graphics", "width", "300")
Config.set("graphics", "height", "400")


class NumPad(GridLayout):
    def __init__(self, input_field, **kwargs):
        super(NumPad, self).__init__(**kwargs)
        self.cols = 3
        self.rows = 4
        self.buttons = []
        self.input_field = input_field

        for i in range(0, 11):
            button = Button(
                font_size=25,
                background_color=[1, 1, 1, 1],
                on_press=self.bAction)
            if (i < 9):
                button.text = str(i + 1)
            elif i == 9:
                button.text = '0'
            elif i == 10:
                button.text = '.'
            self.buttons.append(button)
            self.add_widget(button)

        self.equal_btn = Button(
            text='=',
            font_size=25,
            background_color=[1, 1, 1, 1],
            on_press=self.calculate)
        self.buttons.append(self.equal_btn)
        self.add_widget(self.equal_btn)

    def bAction(self, button):
        self.input_field.text += button.text

    def calculate(self, btn):
        self.input_field.text = str(calc.calc(self.input_field.text))


class OperationPad(GridLayout):
    def __init__(self, input_field, **kwargs):
        super(OperationPad, self).__init__(**kwargs)
        self.cols = 3
        self.rows = 4

        self.input_field = input_field

        self.plus_button = Button(
            text="+",
            font_size=25,
            background_color=[1, 1, 1, 1],
            on_press=self.bAction)
        self.add_widget(self.plus_button)

        self.clear_button = Button(
            text="<-",
            font_size=25,
            background_color=[1, 1, 1, 1],
            on_press=self.clear_char)
        self.add_widget(self.clear_button)

        self.clear_all_btn = Button(
            text="ac",
            font_size=25,
            background_color=[1, 1, 1, 1],
            on_press=self.clear_all)
        self.add_widget(self.clear_all_btn)

        self.minus_button = Button(
            text="-",
            font_size=25,
            background_color=[1, 1, 1, 1],
            on_press=self.bAction)
        self.add_widget(self.minus_button)

        self.openB_button = Button(
            text="(",
            font_size=25,
            background_color=[1, 1, 1, 1],
            on_press=self.bAction)
        self.add_widget(self.openB_button)

        self.closeB_button = Button(
            text=")",
            font_size=25,
            background_color=[1, 1, 1, 1],
            on_press=self.bAction)
        self.add_widget(self.closeB_button)

        self.mult_button = Button(
            text="*",
            font_size=25,
            background_color=[1, 1, 1, 1],
            on_press=self.bAction)
        self.add_widget(self.mult_button)

        self.perc_button = Button(
            text="%",
            font_size=25,
            background_color=[1, 1, 1, 1],
            on_press=self.bAction)
        self.add_widget(self.perc_button)

        self.exponentiation = Button(
            text="^",
            font_size=25,
            background_color=[1, 1, 1, 1],
            on_press=self.bAction)
        self.add_widget(self.exponentiation)

        self.div_button = Button(
            text="/",
            font_size=25,
            background_color=[1, 1, 1, 1],
            on_press=self.bAction)
        self.add_widget(self.div_button)

    def bAction(self, button):
        self.input_field.text += button.text

    def clear_char(self, button):
        self.input_field.text = self.input_field.text[:len(self.input_field.
                                                           text) - 1]

    def clear_all(self, btn):
        self.input_field.text = ""


class OperationField(TextInput):
    def __init__(self, **kwargs):
        super(OperationField, self).__init__(**kwargs)
        self.multilines = False
        self.text = ""


class Calculator(App):
    def build(self):
        self.mainLayout = BoxLayout(orientation='vertical')

        self.opf = OperationField()
        self.np = NumPad(self.opf)
        self.op = OperationPad(self.opf)
        self.buttons_layout = BoxLayout(orientation='horizontal')

        self.buttons_layout.add_widget(self.np)
        self.buttons_layout.add_widget(self.op)

        self.mainLayout.add_widget(self.opf)
        self.mainLayout.add_widget(self.buttons_layout)

        return self.mainLayout
