class Stack:
    def __init__(self):
        self.items = []

    @property
    def isEmpty(self):
        return self.items == []

    @property
    def size(self):
        return len(self.items)

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def top(self):
        return self.items[self.size - 1]

    def show_all(self):
        return self.items
