from utils import stack

PRIORITY = {"+": 1, "-": 1, "*": 2, "/": 2, "^": 3, "%": 3}


def calc(expression):

    if not expression:
        raise Exception("Empty expression")

    if type(expression) != str:
        raise TypeError("Wrong argument type...")

    nums = stack.Stack()
    ops = stack.Stack()

    def initiateCalc():
        b_num = float(nums.pop())
        a_num = float(nums.pop())
        op = ops.pop()
        return simple_calculation(a_num, b_num, op)

    expression = str(expression)
    exp = expression.replace(" ", "")

    for i in range(0, len(exp)):

        if exp[i].isdigit() or exp[i] == ".":
            if i > 0 and (exp[i - 1].isdigit() or exp[i - 1] == "."):
                t = nums.pop()
                nums.push(t + exp[i])
            else:
                nums.push(exp[i])
        else:
            if exp[i] in "()":
                if not ops.isEmpty and exp[i] == ")" and ops.top() == "(":
                    ops.pop()
                elif exp[i] == ")" and ops.top() != "(":
                    while ops.top() != "(":
                        print(ops.top())
                        r = initiateCalc()
                        if r:
                            nums.push(r)
                        else:
                            raise Exception("Calculation error")
                    ops.pop()
                else:
                    ops.push(exp[i])
            else:
                if ops.isEmpty or ops.top() == "(":
                    ops.push(exp[i])
                elif exp[i] in PRIORITY.keys():
                    if PRIORITY[exp[i]] > PRIORITY[ops.top()]:
                        ops.push(exp[i])
                    else:
                        while PRIORITY[exp[i]] < PRIORITY[ops.top()]:
                            r = initiateCalc()
                            if r:
                                nums.push(r)
                            else:
                                raise Exception("Calculation error")
                        ops.push(exp[i])
    while not ops.isEmpty:
        r = initiateCalc()
        if r:
            nums.push(r)
        else:
            raise Exception("Calculation error")

    r = nums.pop()
    if float(r).is_integer():
        return int(r)
    else:
        return r


def simple_calculation(a, b, op):
    """Short summary.

    Parameters
    ----------
    a : type
        First number of arithmetic operation.
    b : type
        Second number of arithmetic operation.
    op : type
        Arithmetic operator.

    Returns
    -------
    type
        Float result of arithmetic operation.

    """

    if op == "+":
        return a + b
    if op == "-":
        return a - b
    if op == "*":
        return a * b
    if op == "/":
        return a / b
    if op == "^":
        return a ** b
    if op == "%":
        return a * (b / 100)

    raise Exception("Operation {op} is not found")
